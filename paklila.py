import Adafruit_DHT
import requests
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)

GPIO.setup(21, GPIO.OUT)
#GPIO.output(16, GPIO.HIGH)
#time.sleep(3)

while True:

        humidity, temperature = Adafruit_DHT.read_retry(11, 26)  # GPIO27 (BCM notation)
		data1= temperature
		data2= humidity
		url = 'http://localhost/inputdata.php?data1='+data1+'&data2='+data2+''
		x = requests.get(url) 
		print(x.text)		
		print ("Humidity = {} %; Temperature = {} C".format(humidity, temperature))
        if temperature > 32:
                GPIO.output(21, GPIO.HIGH)
                sleep(3)
        else:
                GPIO.output(21, GPIO.LOW)
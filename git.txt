git config --global user.name "fahrur aslami"
git config --global user.email "fahruraslami@gmail.com"

Create a new repository

git clone git@gitlab.com:aslamifahrur/l.git
cd l
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:aslamifahrur/l.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:aslamifahrur/l.git
git push -u origin --all
git push -u origin --tags

